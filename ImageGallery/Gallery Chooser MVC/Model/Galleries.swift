//
//  Galleries.swift
//  ImageGallery
//
//  Created by Ahmed Sharf on 1/15/22.
//

import Foundation

class Galleries {
    private(set) var galleries = [Gallery]()
    private(set) var deletedGalleries = [Gallery]()
    
    func getAllTitles() -> [String] {
        var arr = [String]()
        galleries.forEach { (gallery) in
            arr.append(gallery.title)
        }
        deletedGalleries.forEach { (gallery) in
            arr.append(gallery.title)
        }
        return arr
    }
    
    func add(_ gallery: Gallery) {
        galleries.append(gallery)
    }
    
    func temporaryDelete(at index: Int) {
        deletedGalleries.append(galleries.remove(at: index))
    }
    
    func permanentDelete(at index: Int) {
        deletedGalleries.remove(at: index)
    }
    
    func undoDeletion(at index: Int) {
        galleries.append(deletedGalleries.remove(at: index))
    }
}
