//
//  GalleryChooserTableViewCell.swift
//  ImageGallery
//
//  Created by Ahmed Sharf on 1/15/22.
//

import UIKit

class GalleryChooserTableViewCell: UITableViewCell {

    @IBOutlet weak private var textField: UITextField! {
        didSet {
            textField.delegate = self
            textField.isEnabled = false
        }
    }
    
    var title: String {
        set {
            textField?.text = newValue
        }
        get {
            return textField.text ?? ""
        }
    }
    
    var resignationHandler: (() -> Void)?
    
    override var isEditing: Bool {
        didSet {
            textField.isEnabled = isEditing
            
            if isEditing == true {
                textField.becomeFirstResponder()
            } else {
                textField.resignFirstResponder()
            }
        }
    }
}

extension GalleryChooserTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        isEditing = false
        resignationHandler?()
    }
}

