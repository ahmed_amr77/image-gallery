//
//  GalleryChooserTableViewController.swift
//  ImageGallery
//
//  Created by Ahmed Sharf on 1/15/22.
//

import UIKit

class GalleryChooserTableViewController: UITableViewController {

    var galleriesModel = Galleries()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        splitViewController?.delegate = self
        splitViewController?.preferredDisplayMode = .oneOverSecondary
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let doubleTabRecognizer = UITapGestureRecognizer(target: self, action: #selector(changeTitle(by:)))
        doubleTabRecognizer.numberOfTapsRequired = 2
        tableView.addGestureRecognizer(doubleTabRecognizer)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNewGallery))
    }
    
    @objc func addNewGallery() {
        let title = "Untitled".madeUnique(withRespectTo: galleriesModel.getAllTitles())
        let gallery = Gallery(title: title)
        galleriesModel.add(gallery)
        tableView.insertRows(at: [IndexPath(row: galleriesModel.galleries.count-1, section: 0)], with: .fade)
    }
    
    // MARK:- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowGallery",
           let galleryVC = segue.destination as? ImageGalleryCollectionViewController,
            let galleryCell = sender as? UITableViewCell {
            
            if let indexPath = tableView.indexPath(for: galleryCell) {
                galleryVC.gallery = galleriesModel.galleries[indexPath.row]
            }
        }
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 1 {
            return false
        }
        return true
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return galleriesModel.deletedGalleries.count
        }
        return galleriesModel.galleries.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GalleryCell", for: indexPath)
        if let galleryCell = cell as? GalleryChooserTableViewCell {
            if indexPath.section == 0 {
                galleryCell.title = galleriesModel.galleries[indexPath.row].title
            } else {
                galleryCell.title = galleriesModel.deletedGalleries[indexPath.row].title
            }
        }
        return cell
    }
    
    @objc func changeTitle(by recognizer: UITapGestureRecognizer) {
        if let indexPath = tableView.indexPathForRow(at: recognizer.location(in: tableView)) {
            let cell = tableView.cellForRow(at: indexPath)
            if let galleryCell = cell as? GalleryChooserTableViewCell {
                galleryCell.resignationHandler = { [weak self, unowned galleryCell] in
                    if indexPath.section == 0 {
                        self?.galleriesModel.galleries[indexPath.row].title = galleryCell.title
                    } else {
                        self?.galleriesModel.deletedGalleries[indexPath.row].title = galleryCell.title
                    }
                    self?.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                    self?.performSegue(withIdentifier: "ShowGallery", sender: cell)
                }
                galleryCell.isEditing = true
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 && galleriesModel.deletedGalleries.count > 0 {
            return "Recently Deleted"
        }
        return ""
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if indexPath.section == 0 {
                galleriesModel.temporaryDelete(at: indexPath.row)
                tableView.moveRow(at: indexPath, to: IndexPath(row: galleriesModel.deletedGalleries.count-1, section: 1))
            } else {
                galleriesModel.permanentDelete(at: indexPath.row)
                tableView.performBatchUpdates {
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    tableView.reloadSections(IndexSet(indexPath), with: .fade)
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if indexPath.section == 1 {
            return UISwipeActionsConfiguration(actions: [UIContextualAction(style: .normal, title: "Undo", handler: { [weak self] _,_,_ in
                guard let self = self else { return }
                self.galleriesModel.undoDeletion(at: indexPath.row)
                self.tableView.reloadData()
            })])
        }
        return nil
    }
}

extension GalleryChooserTableViewController: UISplitViewControllerDelegate {
    func splitViewController(_ svc: UISplitViewController, topColumnForCollapsingToProposedTopColumn proposedTopColumn: UISplitViewController.Column) -> UISplitViewController.Column {
        return .primary
    }
}
