//
//  ImageGalleryCollectionViewCell.swift
//  ImageGallery
//
//  Created by Ahmed Sharf on 1/14/22.
//

import UIKit

class ImageGalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak private var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak private var imageView: UIImageView!
    
    var image: UIImage? {
        didSet {
            imageView.image = image
            if image != nil {
                spinner.stopAnimating()
            }
        }
    }
}
