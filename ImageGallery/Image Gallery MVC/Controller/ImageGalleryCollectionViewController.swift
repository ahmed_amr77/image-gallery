//
//  ImageGalleryCollectionViewController.swift
//  ImageGallery
//
//  Created by Ahmed Sharf on 1/14/22.
//

import UIKit

class ImageGalleryCollectionViewController: UICollectionViewController {
    
    var gallery = Gallery(title: "Test")
    
    var flowLayout: UICollectionViewFlowLayout? {
        return collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
    }
    private var minimumItemWidth: CGFloat {
        return (collectionView.frame.size.width / 3) - (itemSpacing * 2)
    }
    private var maximumItemWidth: CGFloat {
        return collectionView.frame.size.width - 5
    }
    let itemSpacing: CGFloat = 5
    var itemWidth: CGFloat = 0 {
        didSet {
            if itemWidth > maximumItemWidth {
                itemWidth = maximumItemWidth
            }
            else if itemWidth < minimumItemWidth {
                itemWidth = minimumItemWidth
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //to remove the initial controller that created with "Test title" when navigate to new Gallery.
        guard let sc = splitViewController?.viewControllers,
              let vc = (sc.last as? UINavigationController)?.viewControllers.first,
              title != "Test",
              vc.title == "Test" else { return }
        navigationController?.viewControllers.removeFirst()
        navigationItem.setHidesBackButton(true, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        
        title = gallery.title
        
        itemWidth = (collectionView.frame.size.width / 2) - itemSpacing

        collectionView.dropDelegate = self
        collectionView.dragDelegate = self
        
        collectionView.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(self.resizeSelectedLabel(by:))))
    }
    
    @objc func resizeSelectedLabel(by recognizer: UIPinchGestureRecognizer) {
        switch recognizer.state {
        case .began, .changed:
            itemWidth *= recognizer.scale
            flowLayout?.invalidateLayout()
            recognizer.scale = 1.0
        default:
            break
        }
    }
    
    // MARK:- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowImage",
            let imageVC = segue.destination as? ImageViewController,
            let imageCell = sender as? UICollectionViewCell {
            
            if let indexPath = collectionView.indexPath(for: imageCell) {
                imageVC.url = gallery.images[indexPath.item].url
            }
        }
    }

    // MARK:- UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gallery.images.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageGalleryCollectionViewCell", for: indexPath)
        if let imageCell = cell as? ImageGalleryCollectionViewCell {
            DispatchQueue.global(qos: .userInitiated).async {
                if let url = self.gallery.images[indexPath.item].url {
                    let image = url.fetchImage()
                    DispatchQueue.main.async {
                        imageCell.image = image
                    }
                }
            }
        }
        return cell
    }
}

// MARK:- FlowLayout
extension ImageGalleryCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemHeight = itemWidth / gallery.images[indexPath.item].ratio
        return CGSize(width: itemWidth, height: itemHeight)
    }
}

// MARK:- Drop
extension ImageGalleryCollectionViewController: UICollectionViewDropDelegate {
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        let isSelf = (session.localDragSession?.localContext) as? UICollectionView == collectionView
        return isSelf ? session.canLoadObjects(ofClass: UIImage.self) : session.canLoadObjects(ofClass: NSURL.self) && session.canLoadObjects(ofClass: UIImage.self)
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        let isSelf = (session.localDragSession?.localContext) as? UICollectionView == collectionView
        return UICollectionViewDropProposal(operation: isSelf ? .move : .copy, intent: .insertAtDestinationIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: gallery.images.count, section: 0)
        for item in coordinator.items {
            if let sourceIndexPath = item.sourceIndexPath {
                collectionView.performBatchUpdates {
                    gallery.move(from: sourceIndexPath.item, to: destinationIndexPath.item)
                    collectionView.deleteItems(at: [sourceIndexPath])
                    collectionView.insertItems(at: [destinationIndexPath])
                }
                coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
            } else {
                let placeholderContext = coordinator.drop(item.dragItem, to: UICollectionViewDropPlaceholder(insertionIndexPath: destinationIndexPath, reuseIdentifier: "PlaceholderCell"))
                var url: URL?
                item.dragItem.itemProvider.loadObject(ofClass: NSURL.self) { (urlProvider, _) in
                    if let draggedURL = urlProvider as? NSURL {
                        url = draggedURL as URL
                    } else {
                        DispatchQueue.main.async {
                            placeholderContext.deletePlaceholder()
                        }
                    }
                }
                item.dragItem.itemProvider.loadObject(ofClass: UIImage.self) { [weak self] (imageProvider, _) in
                    DispatchQueue.main.async {
                        if let image = imageProvider as? UIImage {
                            placeholderContext.commitInsertion { (insertionIndexPath) in
                                self?.gallery.add(image, with: url, at: insertionIndexPath)
                            }
                        }  else {
                            placeholderContext.deletePlaceholder()
                        }
                    }
                }
            }
        }
    }
}

// MARK:- Drag
extension ImageGalleryCollectionViewController: UICollectionViewDragDelegate {
    private func dragItem(at indexPath: IndexPath) -> [UIDragItem] {
        if let image = (collectionView.cellForItem(at: indexPath) as? ImageGalleryCollectionViewCell)?.image {
            let dragItem = UIDragItem(itemProvider: NSItemProvider(object: image))
            dragItem.localObject = image
            return [dragItem]
        } else {
            return []
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        session.localContext = collectionView
        return dragItem(at: indexPath)
    }
    
    //drag another items while dragging
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        return dragItem(at: indexPath)
    }
}
