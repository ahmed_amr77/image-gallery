//
//  Gallery.swift
//  ImageGallery
//
//  Created by Ahmed Sharf on 1/14/22.
//

import UIKit

class Gallery {
    private(set) var images = [Image]()
    var title: String
    
    init(title: String) {
        self.title = title
    }
    
    func add(_ image: UIImage, with url: URL?, at index: IndexPath) {
        let ratio = image.size.width / image.size.height
        images.insert(Image(url: url, ratio: ratio), at: index.item)
    }
    
    func move(from sourceIndex: Int, to destIndex: Int) {
        let movedItem = images.remove(at: sourceIndex)
        if destIndex < (images.count)  {
            images.insert(movedItem, at: destIndex)
        } else {
            images.append(movedItem)
        }
    }
    
    func delete(at index: Int) {
        images.remove(at: index)
    }
}
