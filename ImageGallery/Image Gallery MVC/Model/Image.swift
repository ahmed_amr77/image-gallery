//
//  Image.swift
//  ImageGallery
//
//  Created by Ahmed Sharf on 1/14/22.
//

import UIKit

struct Image {
    let url: URL?
    let ratio: CGFloat
}
