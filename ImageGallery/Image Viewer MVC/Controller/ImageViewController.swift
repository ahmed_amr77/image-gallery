//
//  ImageViewController.swift
//  ImageGallery
//
//  Created by Ahmed Sharf on 1/14/22.
//

import UIKit

class ImageViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.minimumZoomScale = 0.5
            scrollView.maximumZoomScale = 5.0
            scrollView.delegate = self
            scrollView.addSubview(imageView)
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var url: URL?

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        fetchImage()
    }
    
    private func fetchImage() {
        spinner.startAnimating()
        guard let url = url else { return }
        
        DispatchQueue.global(qos: .userInitiated).async {
            let image = url.fetchImage()
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.spinner.stopAnimating()
                self.imageView.image = image
                self.imageView.sizeToFit()
                self.scrollView.contentSize = self.imageView.frame.size
            }
        }
    }
}

// MARK:- UIScrollViewDelegate
extension ImageViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        guard scale < 1 else { return }
        centerImage()
    }
    
    private func centerImage() {
        let imgViewSize = imageView.frame.size;
        guard let imageSize = imageView.image?.size else { return }
        
        var realImgSize = CGSize.zero
        if(imageSize.width / imageSize.height > imgViewSize.width / imgViewSize.height) {
            realImgSize = CGSize(width: imgViewSize.width, height: imgViewSize.width / imageSize.width * imageSize.height);
        }
        else {
            realImgSize = CGSize(width: imgViewSize.height / imageSize.height * imageSize.width, height: imgViewSize.height);
        }
        
        var fr = CGRect.zero
        fr.size = realImgSize
        imageView.frame = fr
        
        let scrSize = scrollView.bounds.size;
        let offx = (scrSize.width > realImgSize.width ? (scrSize.width - realImgSize.width) / 2 : 0);
        let offy = (scrSize.height > realImgSize.height ? (scrSize.height - realImgSize.height) / 2 : 0);
        
        UIView.transition(with: scrollView, duration: 0.1, options: .allowAnimatedContent) { [weak self] in
            self?.scrollView.contentInset = UIEdgeInsets(top: offy, left: offx, bottom: offy, right: offx)
        }
    }
}
